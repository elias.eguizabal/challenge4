package com.trainee.domain.interaction

import com.trainee.domain.model.Photo

object DomainUtils {
    val fakePhotosList = listOf(
        Photo(
            1,
            1,
            "Title1",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        Photo(
            1,
            2,
            "Title2",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        Photo(
            1,
            3,
            "Title3",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        Photo(
            1,
            4,
            "Title4",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        )
    )

    val fakePhoto = Photo(
        1,
        5,
        "Title5",
        "https://via.placeholder.com/600/771796",
        "https://via.placeholder.com/150/771796"
    )
}

