package com.trainee.domain.interaction

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.trainee.domain.model.RequestState
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable
import org.junit.Test

class RefreshPhotosUseCaseImplTest {
    private val photoRepository: PhotoRepository = mock()
    private val refreshPhotosUseCase: RefreshPhotosUseCase =
        RefreshPhotosUseCaseImpl(photoRepository)

    @Test
    fun `test refreshPhotos call repository for expected success result`() {
        whenever(photoRepository.refreshPhotos()).thenReturn(Flowable.fromArray(RequestState.Finish))

        refreshPhotosUseCase.refreshPhotos().test().assertValue(RequestState.Finish)
    }
}