package com.trainee.domain.interaction

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.trainee.domain.interaction.DomainUtils.fakePhoto
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable
import org.junit.Test

class GetPhotoUseCaseImplTest {
    private val photoRepository: PhotoRepository = mock()
    private val getPhotoUseCase = GetPhotoUseCaseImpl(photoRepository)

    @Test
    fun `test getPhoto from repository return fakePhoto`() {
        whenever(photoRepository.getPhoto(5)).thenReturn(Flowable.fromArray(fakePhoto))

        getPhotoUseCase.getPhoto(5).test().assertValue(fakePhoto)
    }
}