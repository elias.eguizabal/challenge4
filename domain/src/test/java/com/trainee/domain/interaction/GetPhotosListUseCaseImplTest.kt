package com.trainee.domain.interaction

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.trainee.domain.interaction.DomainUtils.fakePhotosList
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable
import org.junit.Test

class GetPhotosListUseCaseImplTest {
    private val photoRepository: PhotoRepository = mock()
    private val getPhotosListUseCase = GetPhotosListUseCaseImpl(photoRepository)

    @Test
    fun `test getPhotos from repository return fakePhotosList`() {
        whenever(photoRepository.getPhotos()).thenReturn(Flowable.fromArray(fakePhotosList))

        getPhotosListUseCase.getPhotos().test().assertValue(fakePhotosList)
    }
}