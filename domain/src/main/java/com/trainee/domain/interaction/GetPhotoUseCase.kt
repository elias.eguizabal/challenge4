package com.trainee.domain.interaction

import com.trainee.domain.model.Photo
import io.reactivex.Flowable

interface GetPhotoUseCase {
    fun getPhoto(idPhoto: Int): Flowable<Photo>
}