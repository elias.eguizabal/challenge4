package com.trainee.domain.interaction

import com.trainee.domain.model.Photo
import io.reactivex.Flowable

interface GetPhotosListUseCase {
    fun getPhotos(): Flowable<List<Photo>>
}