package com.trainee.domain.interaction

import com.trainee.domain.model.RequestState
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable

class RefreshPhotosUseCaseImpl(private val photoRepository: PhotoRepository) : RefreshPhotosUseCase {
    override fun refreshPhotos(): Flowable<RequestState> = photoRepository.refreshPhotos()
}