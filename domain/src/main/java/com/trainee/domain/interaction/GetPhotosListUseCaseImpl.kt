package com.trainee.domain.interaction

import com.trainee.domain.repository.PhotoRepository

class GetPhotosListUseCaseImpl(private val photoRepository: PhotoRepository) :
    GetPhotosListUseCase {
    override fun getPhotos() = photoRepository.getPhotos()
}