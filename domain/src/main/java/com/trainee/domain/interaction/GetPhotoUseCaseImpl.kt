package com.trainee.domain.interaction

import com.trainee.domain.model.Photo
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable

class GetPhotoUseCaseImpl(private val photoRepository: PhotoRepository) : GetPhotoUseCase {
    override fun getPhoto(idPhoto: Int): Flowable<Photo> = photoRepository.getPhoto(idPhoto)
}
