package com.trainee.domain.interaction

import com.trainee.domain.model.RequestState
import io.reactivex.Flowable

interface RefreshPhotosUseCase {
    fun refreshPhotos(): Flowable<RequestState>
}