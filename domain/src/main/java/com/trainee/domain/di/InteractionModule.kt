package com.trainee.domain.di

import com.trainee.domain.interaction.*
import org.koin.dsl.module

val interactionModule = module {
    factory<GetPhotosListUseCase> { GetPhotosListUseCaseImpl(photoRepository = get()) }
    factory<GetPhotoUseCase> { GetPhotoUseCaseImpl(photoRepository = get()) }
    factory<RefreshPhotosUseCase> { RefreshPhotosUseCaseImpl(photoRepository = get()) }
}