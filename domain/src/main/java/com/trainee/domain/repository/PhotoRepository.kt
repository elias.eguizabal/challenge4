package com.trainee.domain.repository

import com.trainee.domain.model.Photo
import com.trainee.domain.model.RequestState
import io.reactivex.Flowable

interface PhotoRepository {
    fun getPhotos(): Flowable<List<Photo>>
    fun refreshPhotos(): Flowable<RequestState>
    fun getPhoto(idPhoto: Int): Flowable<Photo>
}