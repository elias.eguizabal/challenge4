package com.trainee.data.repository

import com.trainee.data.BaseTestData
import com.trainee.domain.model.RequestState
import org.junit.Test
import java.net.HttpURLConnection


class PhotoRepositoryImplTest : BaseTestData() {

    //getPhotos Test
    @Test
    fun `test getPhotos list from repository for expected empty list`() {
        photoRepository.getPhotos().test().assertValue(emptyList())
    }

    //refreshPhotos Test
    @Test
    fun `test refreshPhotos call repository and return success`() {
        mockNetworkResponseWithFileContent("success_resp_list.json", HttpURLConnection.HTTP_OK)
        photoRepository.refreshPhotos().test().assertValue(RequestState.Finish)
        photoRepository.getPhotos().test().assertValue(expectedList)
    }

    //getPhoto Test
    @Test
    fun `test getPhoto call repository and return photo whit id 1`() {
        mockNetworkResponseWithFileContent("success_resp_list.json", HttpURLConnection.HTTP_OK)
        photoRepository.refreshPhotos().test()
        val expected = expectedList.first { it.photoId == 1 }
        photoRepository.getPhoto(1).test().assertValue(expected)
    }

}