package com.trainee.data.di

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.trainee.data.database.DataBase
import org.koin.dsl.module

val databaseTestModule = module {
    single { ApplicationProvider.getApplicationContext<Context>() }
    single {
        Room.inMemoryDatabaseBuilder(get(), DataBase::class.java)
            .allowMainThreadQueries()
            .build()
    }
    factory { get<DataBase>().photoDao() }
}