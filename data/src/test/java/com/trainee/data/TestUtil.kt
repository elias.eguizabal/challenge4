package com.trainee.data

import com.trainee.data.database.model.PhotoEntity

object DataUtils {
    val fakePhotosEntityList = listOf(
        PhotoEntity(
            1,
            1,
            "Title1",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        PhotoEntity(
            1,
            2,
            "Title2",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        PhotoEntity(
            1,
            3,
            "Title3",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        ),
        PhotoEntity(
            1,
            4,
            "Title4",
            "https://via.placeholder.com/600/92c952",
            "https://via.placeholder.com/150/92c952"
        )
    )
}