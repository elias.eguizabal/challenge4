package com.trainee.data

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity
import com.trainee.data.di.configureNetworkModuleForTest
import com.trainee.data.di.databaseTestModule
import com.trainee.data.di.mockWebServerTest
import com.trainee.data.di.repositoryModule
import com.trainee.domain.model.Photo
import com.trainee.domain.repository.PhotoRepository
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.File

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
open class BaseTestData : KoinTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var mMockServerInstance: MockWebServer

    private var mShouldStart = false

    protected val photoDao: PhotoDao by inject()
    protected val photoRepository: PhotoRepository by inject()

    lateinit var expectedList: List<Photo>
    private lateinit var photoEntityList: List<PhotoEntity>

    @Before
    open fun setUp() {
        startMockServer(true)
        configureDi()
        val sampleResponse = getJson("success_resp_list.json")
        val photoArray: Array<PhotoEntity> =
            Gson().fromJson(sampleResponse, Array<PhotoEntity>::class.java)
        photoEntityList = photoArray.asList().subList(0, 25)
        expectedList = photoEntityList.map { it.mapToDomainModel() }
    }

    fun mockNetworkResponseWithFileContent(fileName: String, responseCode: Int) =
        mMockServerInstance.enqueue(
            MockResponse()
                .setResponseCode(responseCode)
                .setBody(getJson(fileName))
        )

    private fun getJson(path: String): String {
        val uri = javaClass.classLoader!!.getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }

    private fun startMockServer(shouldStart: Boolean) {
        if (shouldStart) {
            mShouldStart = shouldStart
            mMockServerInstance = MockWebServer()
            mMockServerInstance.start()
        }
    }

    private fun getMockWebServerUrl() = mMockServerInstance.url("/").toString()

    private fun stopMockServer() {
        if (mShouldStart) {
            mMockServerInstance.shutdown()
        }
    }

    @After
    open fun tearDown() {
        stopMockServer()
        stopKoin()
    }

    private fun configureDi() {
        startKoin {
            modules(
                listOf(
                    databaseTestModule,
                    configureNetworkModuleForTest(getMockWebServerUrl()),
                    repositoryModule,
                    mockWebServerTest
                )
            )
        }
    }
}