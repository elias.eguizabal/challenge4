package com.trainee.data.database

import com.trainee.data.BaseTestData
import com.trainee.data.DataUtils.fakePhotosEntityList
import org.junit.Test


class PhotoDaoTest : BaseTestData() {

    override fun setUp() {
        super.setUp()
        fillDatabase()
    }

    @Test
    fun `test getAll call photoDao return fakePhotosEntityList`() {
        clearDatabase()
        fillDatabase()

        photoDao.getAll().test().assertValue(fakePhotosEntityList)
    }

    @Test
    fun `test getAll call photoDao return emptyList`() {
        clearDatabase()

        photoDao.getAll().test().assertValue(emptyList())
    }

    @Test
    fun `test getPhoto call photoDao return PhotoID=2`() {
        clearDatabase()
        fillDatabase()

        val expected = fakePhotosEntityList.first { it.photoId == 2 }
        photoDao.getPhoto(2).test().assertValue(expected)
    }

    private fun fillDatabase() {
        photoDao.insertAll(fakePhotosEntityList)
    }

    private fun clearDatabase() {
        photoDao.clear()
    }

}
