package com.trainee.data.di

import com.trainee.data.repository.PhotoRepositoryImpl
import com.trainee.domain.repository.PhotoRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<PhotoRepository> { PhotoRepositoryImpl(photoDao = get(), networkRequests = get()) }
}