package com.trainee.data.di

import androidx.room.Room
import com.trainee.data.database.DataBase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val databaseName = "photo_database"

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            DataBase::class.java,
            databaseName
        ).build()
    }
    factory { get<DataBase>().photoDao() }
}