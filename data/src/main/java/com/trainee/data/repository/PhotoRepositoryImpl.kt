package com.trainee.data.repository

import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.mapToPhotoModel
import com.trainee.data.networking.NetworkRequests
import com.trainee.domain.model.RequestState
import com.trainee.domain.repository.PhotoRepository
import io.reactivex.Flowable

class PhotoRepositoryImpl(
    private val photoDao: PhotoDao,
    private val networkRequests: NetworkRequests
) :
    PhotoRepository {
    override fun getPhotos() =
        photoDao.getAll().mapToPhotoModel()

    override fun refreshPhotos(): Flowable<RequestState> =
        networkRequests.getPhotosList().map { list ->
            if (list.size > 25) {
                list.subList(0, 25)
            } else {
                list
            }
        }.doOnNext {
            photoDao.insertAll(it)
        }.map {
            RequestState.Finish
        }

    override fun getPhoto(idPhoto: Int) =
        photoDao.getPhoto(idPhoto).map { photoEntity ->
            photoEntity.mapToDomainModel()
        }

}
