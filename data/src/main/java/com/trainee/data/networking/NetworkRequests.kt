package com.trainee.data.networking

import com.trainee.data.database.model.PhotoEntity
import io.reactivex.Flowable
import retrofit2.http.GET

interface NetworkRequests {
    @GET(value = "photos")
    fun getPhotosList(): Flowable<List<PhotoEntity>>
}