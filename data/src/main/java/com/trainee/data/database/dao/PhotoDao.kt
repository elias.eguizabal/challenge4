package com.trainee.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.trainee.data.database.model.PhotoEntity
import io.reactivex.Flowable

@Dao
interface PhotoDao {
    @Query("SELECT * FROM photos_table")
    fun getAll(): Flowable<List<PhotoEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(photo: List<PhotoEntity>)

    @Query("SELECT * FROM photos_table WHERE photoId = :photoId LIMIT 1")
    fun getPhoto(photoId: Int): Flowable<PhotoEntity>

    @Query("DELETE FROM photos_table")
    fun clear()
}
