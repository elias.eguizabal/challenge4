package com.trainee.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity

@Database(entities = [PhotoEntity::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}