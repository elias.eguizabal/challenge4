package com.trainee.data.database

import com.trainee.data.database.model.PhotoEntity
import com.trainee.domain.model.Photo
import io.reactivex.Flowable

fun Flowable<List<PhotoEntity>>.mapToPhotoModel(): Flowable<List<Photo>> =
    map { List ->
        List.map { photoEntity ->
            photoEntity.mapToDomainModel()
        }
    }

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}