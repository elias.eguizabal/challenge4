package com.trainee.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.trainee.data.database.DomainMapper
import com.trainee.domain.model.Photo

@Entity(tableName = "photos_table")
data class PhotoEntity(
    val albumId: Int,
    @SerializedName("id")
    @PrimaryKey
    val photoId: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
): DomainMapper<Photo> {
    override fun mapToDomainModel() = Photo(albumId, photoId, title, url, thumbnailUrl)
}