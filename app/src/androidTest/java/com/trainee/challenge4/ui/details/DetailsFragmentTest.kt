@file:Suppress("SpellCheckingInspection")

package com.trainee.challenge4.ui.details

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.trainee.challenge4.R
import com.trainee.data.database.dao.PhotoDao
import com.trainee.data.database.model.PhotoEntity
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest
import org.koin.test.inject

@RunWith(AndroidJUnit4::class)
class DetailsFragmentTest : KoinTest {

    private val photoDao: PhotoDao by inject()

    @Test
    fun test_detailFragment_pass_photoId_return_PhotoInfo_in_UI() = runBlocking {

        photoDao.insertAll(
            listOf(
                PhotoEntity(
                    1000,
                    1000,
                    "Test Title",
                    "https://miro.medium.com/max/1200/1*8TSfE0F1aXcjDb5_WyIEUw.png",
                    "https://miro.medium.com/max/2800/1*xNQHxXBX-1RQCPM3LYa3wA.png"
                )
            )
        )

        val bundle = DetailsFragmentArgs(1000).toBundle()
        launchFragmentInContainer<DetailsFragment>(bundle, R.style.Theme_Challenge4)

        onView(withId(R.id.tv_photo_title)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_photo_title)).check(matches(withText("Test Title")))

        photoDao.clear()
    }

}