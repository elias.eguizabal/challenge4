package com.trainee.challenge4.util

import com.trainee.domain.model.Photo

val fakePhotoListApp = listOf(
    Photo(
        1,
        1,
        "accusamus beatae ad facilis cum similique qui sunt",
        "https://via.placeholder.com/600/92c952",
        "https://via.placeholder.com/150/92c952"
    ),
    Photo(
        1,
        2,
        "reprehenderit est deserunt velit ipsam",
        "https://via.placeholder.com/600/771796",
        "https://via.placeholder.com/150/771796"
    )
)

