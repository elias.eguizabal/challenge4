package com.trainee.challenge4.viewModel.photos

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.trainee.challenge4.util.fakePhotoListApp
import com.trainee.domain.interaction.GetPhotosListUseCase
import com.trainee.domain.interaction.RefreshPhotosUseCase
import com.trainee.domain.model.RequestState
import io.reactivex.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class PhotosMainViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private val getPhotosListUseCase: GetPhotosListUseCase = mock()
    private val refreshPhotosUseCase: RefreshPhotosUseCase = mock()
    private val photosMainViewModel =
        PhotosMainViewModel(getPhotosListUseCase, refreshPhotosUseCase)

    @Test
    fun `Test subscribeToPhotos call getPhotosListUseCase return entire photoList`() {
        whenever(getPhotosListUseCase.getPhotos()).thenReturn(Flowable.fromArray(fakePhotoListApp))
        photosMainViewModel.subscribeToPhotos().test().assertValue(fakePhotoListApp)
    }

    @Test
    fun `Test refreshPhotos call refreshPhotosUseCase return entire photoList`() {
        whenever(refreshPhotosUseCase.refreshPhotos()).thenReturn(Flowable.fromArray(RequestState.Finish))
        photosMainViewModel.refreshPhotos().test().assertValue(RequestState.Finish)
    }

}
