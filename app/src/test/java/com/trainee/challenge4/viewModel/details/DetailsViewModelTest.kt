package com.trainee.challenge4.viewModel.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.trainee.challenge4.util.fakePhotoListApp
import com.trainee.domain.interaction.GetPhotoUseCase
import io.reactivex.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class DetailsViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private val getPhotoUseCase: GetPhotoUseCase = mock()
    private val detailsViewModel = DetailsViewModel(getPhotoUseCase)

    //getPhotosFromLocal viewModel

    @Test
    fun `Test getPhoto call getPhotoUseCase produce photos liveData change`() {
        whenever(getPhotoUseCase.getPhoto(1)).thenReturn(Flowable.fromArray(fakePhotoListApp[0]))
        detailsViewModel.getPhoto(1).test().assertValue(fakePhotoListApp[0])
    }

}