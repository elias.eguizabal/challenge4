package com.trainee.challenge4.util

import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders


fun glideUrl(urlPrev: String) = GlideUrl(
    urlPrev, LazyHeaders.Builder()
        .addHeader("User-Agent", "your-user-agent")
        .build()
)