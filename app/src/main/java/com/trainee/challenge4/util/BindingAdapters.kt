package com.trainee.challenge4.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.trainee.challenge4.R

@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(Url: String?) {
    val placeholder = CircularProgressDrawable(this.context).apply {
        strokeWidth = 5f
        centerRadius = 30f
        start()
    }
    Url?.let {
        val glideUrl = glideUrl(Url)
        Glide.with(this.context)
            .load(glideUrl)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .placeholder(placeholder)
            .error(
                Glide.with(this.context).load(glideUrl).placeholder(placeholder)
                    .error(R.drawable.ic_baseline_error_24)
            )
            .into(this)
    }

}
