package com.trainee.challenge4.viewModel.details

import androidx.lifecycle.ViewModel
import com.trainee.domain.interaction.GetPhotoUseCase

class DetailsViewModel(private val getPhotoUseCase: GetPhotoUseCase) : ViewModel() {

    fun getPhoto(idPhoto: Int) = getPhotoUseCase.getPhoto(idPhoto)
}