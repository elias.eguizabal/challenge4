package com.trainee.challenge4.viewModel.photos

import androidx.lifecycle.ViewModel
import com.trainee.domain.interaction.GetPhotosListUseCase
import com.trainee.domain.interaction.RefreshPhotosUseCase

class PhotosMainViewModel(
    private val getPhotosListUseCase: GetPhotosListUseCase,
    private val refreshPhotosUseCase: RefreshPhotosUseCase
) : ViewModel() {

    var firstRun = true

    fun subscribeToPhotos() = getPhotosListUseCase.getPhotos()

    fun refreshPhotos() = refreshPhotosUseCase.refreshPhotos()
}
