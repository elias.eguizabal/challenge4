@file:Suppress("unused")

package com.trainee.challenge4

import android.app.Application
import com.trainee.challenge4.di.presentationModule
import com.trainee.data.di.databaseModule
import com.trainee.data.di.networkingModule
import com.trainee.data.di.repositoryModule
import com.trainee.domain.di.interactionModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

open class Challenge4 : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@Challenge4)
            modules(provideDependency())
        }
    }

    private val appComponent = appModules + domainModules + dataModules
    open fun provideDependency() = appComponent

}

val appModules = listOf(presentationModule)
val domainModules = listOf(interactionModule)
val dataModules = listOf(databaseModule, networkingModule, repositoryModule)
