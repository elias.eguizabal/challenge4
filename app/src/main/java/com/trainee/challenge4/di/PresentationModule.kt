package com.trainee.challenge4.di

import com.trainee.challenge4.viewModel.details.DetailsViewModel
import com.trainee.challenge4.viewModel.photos.PhotosMainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { PhotosMainViewModel(getPhotosListUseCase = get(), refreshPhotosUseCase = get()) } //getPhotosListUseCase =
    viewModel { DetailsViewModel(getPhotoUseCase = get()) }
}