package com.trainee.challenge4.ui.photos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.trainee.challenge4.R
import com.trainee.challenge4.databinding.PhotoItemBinding
import com.trainee.domain.model.Photo

class PhotosAdapter(
    private val photoClickListener: PhotoItemClickListener
) :
    RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder>() {

    private var dataList = listOf<Photo>()

    fun setListData(data: List<Photo>?) {
        if (data != null) {
            dataList = data
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder =
        PhotoViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.photo_item,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.photoItemBinding.photo = dataList[position]
        holder.photoItemBinding.clickListener = photoClickListener
    }

    override fun getItemCount() = dataList.size

    inner class PhotoViewHolder(val photoItemBinding: PhotoItemBinding) :
        RecyclerView.ViewHolder(photoItemBinding.root)

    interface PhotoItemClickListener {
        fun onPhotoItemClicked(photo: Photo)
    }

}
