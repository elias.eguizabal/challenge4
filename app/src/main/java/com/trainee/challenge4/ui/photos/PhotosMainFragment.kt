package com.trainee.challenge4.ui.photos

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.trainee.challenge4.databinding.PhotosMainFragmentBinding
import com.trainee.challenge4.viewModel.photos.PhotosMainViewModel
import com.trainee.domain.model.Photo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.ext.android.viewModel

class PhotosMainFragment : Fragment(), PhotosAdapter.PhotoItemClickListener {

    private lateinit var mainBinding: PhotosMainFragmentBinding
    private val viewModel: PhotosMainViewModel by viewModel()
    private var photosAdapter: PhotosAdapter? = null
    private var noConnection = false
    private var connectivityManager: ConnectivityManager? = null
    private val compositeDisposable = CompositeDisposable()

    companion object {
        const val recoverNetwork = "Connection recovered refresh"
        const val lossNetwork = "Lost connection"
        const val errorNetwork = "Cant connect to internet"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mainBinding = PhotosMainFragmentBinding.inflate(inflater)

        photosAdapter = PhotosAdapter(this)
        mainBinding.rvPhotosList.adapter = photosAdapter

        connectivityManager =
            activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return mainBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setMainViewObservers()
        setNetworkStateCallbacks()

        subscribeToPhotos()
        if (viewModel.firstRun) {
            checkNetworkToGetPhotos()
            viewModel.firstRun = false
        }
    }

    private fun subscribeToPhotos() {
        compositeDisposable.add(
            viewModel.subscribeToPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    photosAdapter?.setListData(list)
                }
        )
    }

    private fun checkNetworkToGetPhotos() {
        if (hasNetworkAccess()) {
            refreshPhotos()
        } else {
            showToast(errorNetwork)
            noConnection = true
        }
    }

    private fun refreshPhotos() {
        startRefreshing()
        compositeDisposable.add(
            viewModel.refreshPhotos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { endRefreshing() },
                    {
                        endRefreshing()
                        showToast(errorNetwork)
                    }
                )
        )
    }

    private fun hasNetworkAccess() = connectivityManager?.allNetworks?.isNotEmpty() == true

    private fun setNetworkStateCallbacks() {
        val request = NetworkRequest.Builder().build()
        connectivityManager?.registerNetworkCallback(request,
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    if (noConnection) {
                        showToast(recoverNetwork)
                        noConnection = false
                    }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    showToast(lossNetwork)
                    noConnection = true
                }
            })
    }

    private fun setMainViewObservers() {
        mainBinding.refreshLayout.setOnRefreshListener {
            checkNetworkToGetPhotos()
        }
    }

    private fun startRefreshing() {
        mainBinding.refreshLayout.isRefreshing = true
    }

    private fun endRefreshing() {
        mainBinding.refreshLayout.isRefreshing = false
    }

    override fun onPhotoItemClicked(photo: Photo) {
        findNavController().navigate(
            PhotosMainFragmentDirections.actionMainToDetails(photo.photoId)
        )
    }

    private fun showToast(message: String) {
        context?.let {
            Toast.makeText(it, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
